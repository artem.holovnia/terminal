import pathlib
from helpers import Parameter

BASE_DIR = pathlib.Path(__file__).parent
SECRET_KEY = 'IWcW4373_zvepElhZvZqNAQ1PSORR4lUCtN982_OeBY='
ALLOWED_PARAMETERS = [
    Parameter(0, 'Name', str, True),
    Parameter(1, 'Age', int, True),
    Parameter(2, 'City', str, False),
]
PARAMETR_DATA_TYPE_MAP = {
    'str': 'text',
    'int': 'number'
}
DATABASE = {
    'DB_USER':'terminal',
    'DB_PASSWORD':'terminal',
    'DB_NAME':'terminal',
    'DB_HOST':'localhost',
    'DB_PORT':3306
}